[![pipeline status](https://gitlab.cern.ch/LAr_LTDB_DNL/LAr_LTDB_DNL.gitlab.io/badges/master/pipeline.svg)](https://cern.gitlab.ch/LAr_LTDB_DNL/cern.gitlab.ch/-/commits/master)

[![coverage report](https://gitlab.cern.ch/LAr_LTDB_DNL/LAR_LTDB_DNL.gitlab.io/badges/master/coverage.svg)](https://cern.gitlab.ch/LAr_LTDB_DNL/cern.gitlab.ch/-/commits/master)

---

Repository for checking the installed LTDB performance regarding its DNL. 

The site (after the pipeline passed) is acessible here : https://LAr_LTDB_DNL.gitlab.ch/


---

Get a recent Python version on lxplux or other ATLAS system :
asetup AnalysisBase,22.2.9

Install some extra packages 

pip install --user alive-progress
pip install --user tabulate
pip install --user coloredlogs
pip install --user root_numpy


/eos/home-l/lardaq/P1Results/LATOMERun_Weekly_210319-145103/LArRamp_00389763.root


Or if using pipenv :

pipenv shell

pipenv install numpy root_numpy alive_progress tabulate coloredlogs

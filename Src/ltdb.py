"""Code to extract the performance plots from LTDB calibration runs.

    Marco leite
    leite@cern.ch
    April 2021
"""

import os
import itertools
import logging 
import coloredlogs
import tabulate
import numpy as np 
from alive_progress import alive_bar
from numba import jit

import ROOT
from root_numpy import root2array, tree2array


class ltdb :
    """This class reads a calibration file and extracts several performance numbers for LTDBs.
    """
    #===================================================================

    #===================================================================
    def __init__(self) :
        """etc.
        """
        self.init_logging()
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptFit(11111)
        ROOT.gStyle.SetPalette(ROOT.kDarkBodyRadiator)

    #===================================================================

    #===================================================================
    def read_file(self, fName=None, branch="RAMPS") :
        """Opens and prepares for reading the calibration file, maybe also outpur some stats from it.

        Args:
            fName (string, optional): The calibration ROOT file name. Defaults to None.
            branch (string, optional): The branch from root file to use. Defaults to "RAMPS"
        """
        self.lg.info(f"Reading {fName}")
        tFile = ROOT.TFile(fName) 

        self.lg.info(f"Getting branch {branch}")
        tree = tFile.Get(branch)

        ne = tree.GetEntries()
        self.lg.info(f"Branch {branch} has {ne} entries")

        #Create an entry in a directory to save the plots
        ff = os.path.basename(fName).replace(".root","")
        self.plotFileName = f"../Plots/{ff}.pdf"

        self.lg.info(f"Saving plots into {self.plotFileName}")
       
        
        #Seem that when dac index == 0 information is not filled
        #FIXME: this is supposition

        if branch == "RAMPS" :
            self.data = tree2array(tree,["DACIndex", "DAC", "ADC", "channelId"])

            z = self.data["DACIndex"]
            i_valid = np.where (z != 0)


            self.DACIndex  = z[i_valid]
            self.DAC       = self.data["DAC"][i_valid]
            self.ADC       = self.data["ADC"][i_valid]
            self.channelID = self.data["channelId"][i_valid]
        
        if branch == "LARDIGITS" :
            self.data = tree2array(tree,["samples", "channelId", "FT", "channel", "barrel_ec", "pos_neg"])

            #Extract the channels and FT information
            #try to use np.unique
            channel = np.unique (self.data["channel"])
            ft = np.unique (self.data["FT"])
            side = np.unique (self.data["pos_neg"])
            region = np.unique (self.data["barrel_ec"])
        
            self.account = {}
            self.account["channel"] = channel
            self.account["ft"] = ft
            self.account["side"] = side
            self.account["region"] = region

            #This is not trully correct because FT numbers will repeat 
            #across side, region
            self.lg.info(f"Unique sides : {side}")
            self.lg.info(f"Unique regions : {region}")
            self.lg.info(f"Unique FTs : {ft} ")
            self.lg.info(f"Unique Channels: {channel} ")

            #Here we should assemble the array as s[evt, ft, ch, sample] 
            #ss = np.stack((s01,s02),axis=2)
            self.xxx = {}
            self.mean = {}
            self.std  = {}
            
            for f in ft :
                self.lg.info(f"Scanning FT {f} ")
                with alive_bar(len(channel),spinner="dots_reverse") as bar :
                    for c in channel :
                        idx = np.where( (self.data["FT"] == f) & 
                                        (self.data["channel"] == c) )

                        s = self.data["samples"][idx]
                        try : 
                            self.xxx[f] = np.dstack((self.xxx[f],s))
                        except KeyError :
                            self.xxx[f] = s
                        bar()
 
                    self.lg.info(f"Final shape array : {self.xxx[f].shape}")
                    self.mean[f] = np.mean(self.xxx[f],axis=0)
                    self.std[f]  = np.std(self.xxx[f],axis=0)

    #===================================================================
    #===================================================================
    def plot_noise(self, FT=None, region=None, side=None) :
        """ Takes information from 1st sample and plot the noise information. 
        """

        sample = 0
        self.plot = {}
        x = self.account["channel"].astype(float)
        n = x.size

        canvas = ROOT.TCanvas()
        canvas.Print(self.plotFileName + "[","pdf")

        for f in FT :
            y = self.std[f][sample]
            graph = ROOT.TGraph(n, x, y )
            graph.Draw("AP*")
            tit = f"Side={side}_Region={region}_FT={f}_Sample={sample}"
            self.lg.info(f"Plotting {tit}")

            graph.SetTitle(tit)
            graph.GetXaxis().SetTitle("Channel #")
            graph.GetYaxis().SetTitle("\sigma_{Pedestal}")

            ROOT.gPad.Update()
            canvas.Print(self.plotFileName)

            #1D histogram 
            tit = f"h_Side={side}_Region={region}_FT={f}_Sample={sample}"
            h_std = ROOT.TH1F(tit,tit,100,0,2)
            for i in y :
                h_std.Fill(i)
            #map(h_std.Fill,y)
            canvas.SetLogy(1)
            h_std.Draw("hist")
            h_std.GetXaxis().SetTitle("\sigma_{Pedestal}")
            h_std.GetYaxis().SetTitle("Entries")
            ROOT.gPad.Update()
            canvas.Print(self.plotFileName)
            canvas.SetLogy(0)

            #All samples together
            ny,nx = self.std[f].shape
            self.ht = ROOT.TH2F(tit,tit,nx,0,nx-1,ny,0,ny-1)
            
            for j in range(ny) :
                for i in range(nx) :
                    w = self.std[f][j][i]
                    self.ht.Fill(i,j,w)
            
            self.ht.Draw("color,z")
            self.ht.GetXaxis().SetTitle("Channel #")
            self.ht.GetYaxis().SetTitle("ADC Sample")
            self.ht.GetYaxis().SetTitle("ADC Sample")
            ROOT.gPad.Update()
            canvas.Print(self.plotFileName)

            #Coherent noise
            hcn = self.get_coherent_noise(f,region, side)
            #canvas.SetLogz(1)
            hcn.Draw("color,z")
            hcn.GetXaxis().SetTitle("Channel #")
            hcn.GetYaxis().SetTitle("Channel #")
            ROOT.gPad.Update()
            canvas.Print(self.plotFileName)
            #canvas.SetLogz(0)
        canvas.Print(self.plotFileName + "]")

    #===================================================================
    #===================================================================
    def get_coherent_noise(self,FT=None, region=None, side=None) :
        """
        Calculate the coherent noise in each channel, 
        them fill the 2d histo
            \rho_{xy} = \frac{\sigma_{xy}}{\sigma_x\sigma_y},  \quad \quad \sigma_{xy}= \frac{1}{N}\sum_{i=1}^{n}(x_i-\mu_x)(y_i-\mu_y)

        """
        sample = 0
        #Get the pedestal subtracted
        ped = self.xxx[FT][:,sample,:] - self.mean[FT][sample]
        
        nch = ped.shape[1]
        cb = list(itertools.combinations_with_replacement(range(nch),2))

        tit = f"Side={side}_Region={region}_FT={FT}_Sample={sample}"
        hist = ROOT.TH2F(tit,tit,nch,0,nch-1,nch,0,nch-1)

        ped_mean = np.mean(ped,axis=0)
        pp = ped - ped_mean
        pp_std = np.std(pp,axis=0)
        with alive_bar(len(cb),spinner="waves") as bar :
            for (i,j) in cb :

                mp = pp[:,i] * pp[:,j]
                cov = mp.mean()
                s = pp_std[i] * pp_std[j]
                if (s != 0 ) :
                    corr = cov/s
                else :
                    corr = 0

                #print (" Correlation factor for  channels", (i,j), s1, s2, cov, corr)
                
                if (i != j) :
                    hist.Fill(i,j,corr)
                    hist.Fill(j,i,corr)
                bar()
            
        return hist

    #===================================================================
    #===================================================================
    def get_Channel_data(self) :
        """This will calculate the average value per sample for each channel for a given DAC value
        """

        #Check the data; all ADC should have the same number of samples

        kk = [len(i) for i in self.ADC]

        sample_sizes = set(kk)

        print ("Got these set of sample sizes: ", sample_sizes)
        #for i in np.arange(x.DAC.size) :
        #    print (x.DAC[i][x.DACIndex[i]-1])

        a = tree2array(t, branches=["samples","channelId"])
        b = tree2array(t, branches=["FT","channel"])
        
        id =  a["channelId"]
        
      
    #===================================================================

    #===================================================================
    def init_logging(self) :
        """
            Boilerplate code to intialize the logging facility
        """
        coloredlogs.install()
        self.lg = logging.getLogger("LTDB")
        self.lg.setLevel(logging.DEBUG)

        #File logging
        logFile = '../Log/ltdb.log'
        print (f"*** From now on log will be written to file {logFile}")
        fh = logging.FileHandler(logFile)
        fh.setLevel(logging.DEBUG)
        
        #Terminal logging
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        formatter = coloredlogs.ColoredFormatter('|---> %(levelname)s : %(asctime)s - %(name)s - %(message)s')

        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        self.lg.addHandler(ch)
        self.lg.addHandler(fh)


        #lg.basicConfig(level=lg.DEBUG, filename=logFile, filemode='w', format='%(name)s - %(levelname)s - %(message)s')
    #===============================================================


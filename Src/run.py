"""Main steering routine for LTDB data analysis
"""

import ltdb

#my_fName  = "../CalibRuns/LArRamp_00389763.root"
#my_branch = "RAMPS"

my_fName  = "../CalibRuns/LArDigits_367000.root"
my_branch = "LARDIGITS"


x = ltdb.ltdb()
x.read_file(fName=my_fName, branch=my_branch)
x.plot_noise(FT=[0,1,2,3], region=0, side=0)
#x.get_Channel_data()
    
#do a method to extract information, and another to calculate noise 
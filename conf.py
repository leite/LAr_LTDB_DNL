# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('./repo-src/'))


# -- Project information -----------------------------------------------------

project = 'ATLAS LAr LTDB DNL Performance'
copyright = '2021'
author = 'Marco Leite'

# The full version, including alpha/beta/rc tags
release = '0.0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones
# import edx_theme

# We still need to add suport to C/C++ either thorough breath (doxygen) or sphinx-c-autodoc
extensions = [
    'sphinx.ext.imgconverter',
    'sphinx.ext.todo',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.intersphinx',
    'sphinx.ext.coverage',
    'sphinx.ext.napoleon',
    'sphinx.ext.linkcode',
    'sphinx.ext.viewcode',
    'sphinxcontrib.mermaid',
    'sphinxcontrib.kroki',
    'sphinxcontrib.contentui',
    'sphinxcontrib.wavedrom',
    'sphinx-jsonschema'
]


# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

wavedrom_html_jsinline = False
render_using_wavedrompy = True

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#

html_theme = 'sphinx_rtd_theme'
html_logo = 'Doc/Figures/ATLAS.png'
html_theme_options = {
    'logo_only': False,
    'display_version': True,
}
# html_favicon = os.path.join('Figures', 'html', 'favicon.png')
html_favicon = 'Doc/Figures/ATLAS.png'


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']

def linkcode_resolve(domain, info):
    if domain != 'py':
        return None
    if not info['module']:
        return None
    filename = info['module'].replace('.', '/')
    return "https://LAr_LTDB_DNL.gitlab.cern.ch/Doc/_modules/%s.py" % filename
s